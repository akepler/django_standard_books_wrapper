# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0002_auto_20180201_1228'),
    ]

    operations = [
        migrations.AlterModelOptions(
            name='company',
            options={'permissions': ('change_url', 'Can change api_url'), 'ordering': ('name',)},
        ),
        migrations.RenameField(
            model_name='company',
            old_name='hw_api_url',
            new_name='api_url',
        ),
    ]
