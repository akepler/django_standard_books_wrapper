# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import migrations, models
import django.utils.timezone


class Migration(migrations.Migration):

    dependencies = [
        ('accounts', '0001_initial'),
    ]

    operations = [
        migrations.CreateModel(
            name='Company',
            fields=[
                ('id', models.AutoField(serialize=False, primary_key=True, auto_created=True, verbose_name='ID')),
                ('name', models.CharField(default='No Comapny name specifed', max_length=255, unique=True, verbose_name='Company name')),
                ('hw_api_url', models.URLField(max_length=255, verbose_name='HW API URL')),
                ('date_created', models.DateTimeField(default=django.utils.timezone.now, verbose_name='Date created')),
            ],
            options={
                'ordering': ('name',),
            },
        ),
        migrations.AddField(
            model_name='user',
            name='company',
            field=models.ForeignKey(null=True, to='accounts.Company', related_name='Company'),
        ),
    ]
