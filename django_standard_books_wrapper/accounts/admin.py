from django.contrib import admin
from django.contrib.auth import forms as auth_forms
from django.contrib.auth.admin import UserAdmin
from django.utils.translation import ugettext_lazy as _

from accounts.models import Company, User


class UserChangeForm(auth_forms.UserChangeForm):
    class Meta:
        model = User
        fields = ("email","company",)
    # Hackish variant of builtin UserChangeForm with no username
    def __init__(self, *args, **kwargs):
        super(UserChangeForm, self).__init__(*args, **kwargs)

        if 'username' in self.fields:
            del self.fields['username']


class UserCreationForm(auth_forms.UserCreationForm):
    # Hackish variant of builtin UserCreationForm with email instead of username
    class Meta:
        model = User
        fields = ("email","company",)

    def __init__(self, *args, **kwargs):
        super(UserCreationForm, self).__init__(*args, **kwargs)

        if 'username' in self.fields:
            del self.fields['username']


class CustomUserAdmin(UserAdmin):
    fieldsets = (
        (None, {'fields': ('email', 'password', 'company')}),
        (_('Personal info'), {'fields': ('name',)}),
        (_('Permissions'), {'fields': ('is_active', 'is_staff', 'is_superuser',
                                       'groups', 'user_permissions')}),
        (_('Important dates'), {'fields': ('last_login', 'date_joined')}),
    )
    add_fieldsets = (
        (None, {
            'classes': ('wide',),
            'fields': ('email','company', 'password1', 'password2')}
         ),
    )
    list_display = ('id', 'email', 'name', 'company', 'is_staff')
    search_fields = ('email', 'name')
    ordering = ('email',)

    form = UserChangeForm
    add_form = UserCreationForm


class CompanyAdmin(admin.ModelAdmin):
    list_display = ('name', 'api_url', 'date_created')
    search_fields = ('name',)


admin.site.register(Company, CompanyAdmin)
admin.site.register(User, CustomUserAdmin)
