from django.conf.urls import url
from stock_pickup import views

urlpatterns = [
    url(r'^$', views.StockPickupPageView.as_view(), name='stock'),
    url(r'^settings/', views.StockPickupSettingsView.as_view(), name='stock_settings'),
    # ex: /stock/5/
    url(r'(?P<order_number>\d+)/(?P<order_date>\d+-\d+-\d+)', views.StockOrderPickupPageView.as_view()),
    # ex: /stock/5/results
    url(r'(?P<order_number>\d+)/results/', views.StockPickupPageView.as_view()),
]
