import datetime as DT

from django.forms.models import formset_factory
from crispy_forms.bootstrap import FormActions, FieldWithButtons, PrependedText, PrependedAppendedText, StrictButton, \
    InlineField
from crispy_forms.helper import FormHelper
from crispy_forms.layout import Layout, Submit, Field, Div, Fieldset
from django import forms
from sb.client import StandardBooks




class SettingsInputForm(forms.Form):
    has_permission = False

    def __init__(self, request, *args, **kwargs):
        super(SettingsInputForm, self).__init__(*args, **kwargs)
        if request.user.is_authenticated():
            self.has_permission = request.user.has_perm('accounts.change_company')
            print(request.user.has_perm('accounts.change_company'))
        else:
            raise RuntimeError


        try:
            first_order_date_str = request.session['first_order_date']
        except KeyError:
            today = DT.date.today()
            initial_date = today - DT.timedelta(days=1)
            first_order_date_str = initial_date.strftime("%Y-%m-%d")
        try:
            last_order_date_str = request.session['last_order_date']
        except KeyError:
            today = DT.date.today()
            last_order_date_str = today.strftime("%Y-%m-%d")


        self.fields['first_order_date'] = forms.DateField(label='Tellimused alates',
                                                          initial=first_order_date_str,
                                                          widget=forms.TextInput(attrs={'type': 'date'})
                                                          )
        self.fields['last_order_date'] = forms.DateField(label='Tellimused kuni',
                                                          initial=last_order_date_str,
                                                          widget=forms.TextInput(attrs={'type': 'date'})
                                                          )
        self.fields['api_url'] = forms.URLField(label='API URL',
                                                          initial=request.user.api_url,
                                                          widget=forms.TextInput(attrs={'type': 'url'})
                                                          )
        self.first_order_date = self.fields['first_order_date']
        self.last_order_date = self.fields['last_order_date']
        self.api_url = self.fields['api_url']


    # Uni-form
    helper = FormHelper()
    helper.layout = Layout(
        FieldWithButtons('first_order_date', css_class='span2', ),
        FieldWithButtons('last_order_date', css_class='span1', ),
        InlineField('api_url') if has_permission else InlineField('api_url', readonly=True),
        FormActions(
            Submit('save_changes', 'Apply', css_class="btn-primary"),
        )
    )


class OrderInputForm(forms.Form):
    def __init__(self, request, *args, **kwargs):
        super(OrderInputForm, self).__init__(*args, **kwargs)
        if request.user.is_authenticated():
            StandardBooks.api_url = request.user.api_url
            sb = StandardBooks()

        else:
            raise RuntimeError

        today = DT.date.today()
        try:
            first_order_date_str = request.session['first_order_date']
        except KeyError:
            first_order_date_str = today.strftime("%Y-%m-%d")

        try:
            last_order_date_str = request.session['last_order_date']
        except KeyError:
            last_order_date_str = today.strftime("%Y-%m-%d")

        SB_ORDERS = [tuple(['{}#{}'.format(x[0],x[3]), "#{} {} - [{}] {}".format(x[0],x[3], x[1], x[2])])
                     for x in sb.orders.get_order_numbers_with_customer(first_order_date_str,
                                                                        last_order_date_str)]
        self.fields['order_number'] = forms.IntegerField(label="Tellimuse number",
                                      widget=forms.Select(choices=SB_ORDERS,
                                                          attrs={'onchange': 'this.form.submit()'}
                                                          ))

    # Uni-form
    helper = FormHelper()
    helper.form_class = 'form-horizontal'
    helper.layout = Layout(
        FieldWithButtons('order_number', css_class='span1', ),
        FormActions(
            Submit('save_changes', 'Go!', css_class="btn-primary"),
        )
    )


class OrderRowForm(forms.Form):
    article_code = forms.CharField(label="Artikli kood")
    spec = forms.CharField(label="Nimetus")
    serial = forms.CharField(label="Partii number")
    order_quantity = forms.CharField(label="Tellitud kogus", max_length=4)
    stock_quantity = forms.CharField(label="Kogus laos", max_length=4)


class OrderRowFormSetHelper(FormHelper):

    def __init__(self, *args, **kwargs):
        super(OrderRowFormSetHelper, self).__init__(*args, **kwargs)
        self.form_method = 'post'
        self.form_class = 'table-hover'
        self.template = 'bootstrap/table_inline_formset.html'
        self.render_required_fields = True
        self.layout = Layout(
            InlineField('article_code', readonly=True),
            InlineField('spec', readonly=True),
            InlineField('serial', readonly=True),
            InlineField('order_quantity', readonly=True),
        )
        self.add_input(Submit("submit", "Teeme nii!"))


OrderFormSet = formset_factory(OrderRowForm, extra=0)
