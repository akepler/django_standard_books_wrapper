import datetime as DT

from django.contrib.auth.decorators import login_required
from django.http import HttpResponseRedirect, HttpResponse
from django.utils.decorators import method_decorator
from django.shortcuts import render
from django.views.generic import TemplateView

from .forms import OrderInputForm, OrderFormSet, OrderRowFormSetHelper, OrderRowForm, SettingsInputForm

from sb.client import StandardBooks

class StockPickupSettingsView(TemplateView):
    form_class = SettingsInputForm
    template_name = 'stock_pickup/index.html'

    @method_decorator(login_required)
    def get(self, request, **kwargs):
        form = self.form_class(request)

        return render(request, self.template_name,
                      {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        form = self.form_class(request)
        first_order_date = request.POST.get('first_order_date')
        if first_order_date:
            request.session['first_order_date'] = first_order_date

        last_order_date = request.POST.get('last_order_date')
        if last_order_date:
            request.session['last_order_date'] = last_order_date

        return HttpResponseRedirect('stock/')

        # return render(request, self.template_name, {'form': form})


class StockPickupPageView(TemplateView):
    form_class = OrderInputForm
    template_name = 'stock_pickup/index.html'
    initial = {'orders': ['123', '234']}

    @method_decorator(login_required)
    def get(self, request, **kwargs):
        form = self.form_class(request,initial=self.initial)

        return render(request, self.template_name,
                      {'form': form})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        try:
            form = self.form_class(request)
            order_data = request.POST.get('order_number').split("#")
            order_number = int(order_data[0])
            order_date = DT.datetime.strptime(order_data[1], "%d.%m.%Y" ).strftime("%Y-%m-%d")
            return HttpResponseRedirect('{}/{}'.format(order_number,order_date))
        except AttributeError:
            pass

        return render(request, self.template_name, {'form': form})


class StockOrderPickupPageView(TemplateView):
    template_name = 'stock_pickup/fill_order.html'

    @method_decorator(login_required)
    def get(self, request, **kwargs):

        if request.user.is_authenticated():
            StandardBooks.api_url = request.user.api_url
            sb = StandardBooks()
        else:
            raise RuntimeError
        helper = OrderRowFormSetHelper()
        order_number = kwargs['order_number']
        order_date = kwargs['order_date']
        formset = OrderFormSet(initial=sb.orders.get_order_rows(order_number, order_date))

        return render(request, self.template_name, {'formset': formset,
                                                    'helper': helper,
                                                    'sb_order': order_number})

    @method_decorator(login_required)
    def post(self, request, *args, **kwargs):
        # form = self.form_class(request.POST)
        # if form.is_valid():
        #    order_number = request.POST.get('order_number')
        #    return HttpResponseRedirect('{}/results/'.format(order_number))

        return HttpResponseRedirect('/stock/')
