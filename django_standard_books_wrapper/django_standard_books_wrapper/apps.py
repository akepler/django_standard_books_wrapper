from django.apps import AppConfig
from django.core import checks

from tg_utils.checks import check_production_settings, check_sentry_config


class Django_standard_books_wrapperConfig(AppConfig):
    name = 'django_standard_books_wrapper'
    verbose_name = "Django Standard Books wrapper"

    def ready(self):
        # Import and register the system checks
        checks.register(check_production_settings)
        checks.register(check_sentry_config)
