# StandardBooks

StandardBooks API is accessible only from specified IPs.

## List of available registers

- Kontakti klassifikaatorid - Customer Class - CClassVc
- Artikli klassifikaatorid - Item Class - DIVc
- Objekti klass - Object Class - OTVc
- Objektid - Objects - ObjVc
- Kliendid - Customers - CUVc
- Artiklid - Items - INVc
- Artikliklassid  - Item Groups - ITVc
- Arved - Invoices - IVVc
- Kontod - Accounts - AccVc
- Kanded - Transactions - TRVc
- Müügireskontro ehk laekumata arved - Invoice Balance - ARVC
- Müügitellimused - Sales Orders - ORVC
- Pakkumised - Quotations -QTVC
- Ostureskontro ehk tasumata arved - Purchase invoice balance - APVC
- Käibemaksukoodid - VAT codes - VATCodeBlock
- Tegevuste tüübid - Activity Types - ActTypeVc
- Isikud  - Users - UserVc
- Projektid - PRVc
- Tegevused - Activities - ActVc
- Kliendiklassid - Customer Categories - CCatVc
- Projektieelarved - Project Budgets - TBBUVc
- Laod - Locations - LocationVc
- Arikli hetkeseis - Item Status support register - ItemStatusVc
- Baasvaluuta  - Base Currency Block - BaseCurBlock
- Ettevõtete register - Companies Setting - CompaniesBlock
- Laoklassid - Location Classifications - LocClVc
- Laoklasside tüübid - Location Classification Types - LocClTypeVc
- Laomahakandmised - Stock Depreciations - SDVc
- Laosissetulekud - Goods Receipts - PUVc
- Artikli ajaloo abiregister - Item History Support register - ItemHistVc
- Standardsed probleemid - Standard Problems - StandProblemVc
- Laoliikumised - Stock Movements - StockMovVc
- Teenindustellimused - Service Orders - SVOVc
- Tellimuseklassid - Order Classifications - OrderClassVc
- Seerianumbrid - Serial Numbers - SerBalVc
- Töölehed - Work Sheets - WSVc
- Tuntud seerianumbrid - Known Serial Numbers - SVOSerVc

## Example requests

- [Customers](http://zoobooks.zoovet.ee:2224/WebDataAPI.hal?register=CUVc)
- [Items](http://zoobooks.zoovet.ee:2224/WebDataAPI.hal?register=INVc)

