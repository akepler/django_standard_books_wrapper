import hashlib
import os
from unittest.mock import patch

from django.test import TestCase

from sb.client import StandardBooks
from sb.registers import SBRegister


def fake_fetch_items(self, params=None):
    filename = os.path.join(os.path.dirname(__file__), 'data/item_register.xml')
    with open(filename) as f:
        return f.read()


def fake_fetch_item_status(self, params=None):
    filename = os.path.join(os.path.dirname(__file__), 'data/item_status_register.xml')
    with open(filename) as f:
        return f.read()


def fake_fetch_item_groups(self, params=None):
    filename = os.path.join(os.path.dirname(__file__), 'data/item_group_register.xml')
    with open(filename) as f:
        return f.read()


def fake_fetch_customers(self, params=None):
    filename = os.path.join(os.path.dirname(__file__), 'data/customer_register.xml')
    with open(filename) as f:
        return f.read()


class SBRegisterTestCase(TestCase):

    def setUp(self):
        self.register = SBRegister()

    def test_base_fetch_url(self):
        self.assertEqual(self.register.base_fetch_url, 'http://zoobooks.zoovet.ee:2224/WebDataAPI.hal')

    def test_get_cache_key(self):
        cache_hash = hashlib.sha256('http://zoobooks.zoovet.ee:2224/WebDataAPI.hal:a=1;b=2'.encode('utf-8')).hexdigest()
        self.assertEqual(self.register.get_cache_key({'b': 2, 'a': 1}), 'SB_CACHE:{hash}'.format(hash=cache_hash))

    def test_parse_xml(self):
        self.register.RECORD_ID = 'Code'
        records = fake_fetch_items(self)
        self.assertDictEqual(
            self.register.parse_xml(records),
            {'1001': {'Code': '1001', 'Name': 'Abc'}, '2002': {'Code': '2002', 'Name': 'Xyz'}},
        )


class SBOrderRegisterTestCase(TestCase):

    def setUp(self):
        self.register = StandardBooks().orders

    def test_base_post_url(self):
        self.assertEqual(self.register.base_post_url, 'http://zoobooks.zoovet.ee:2224/WebZVVAPI.hal')


class SBItemRegisterTestCase(TestCase):

    def setUp(self):
        self.register = StandardBooks().items

    @patch.object(SBRegister, 'fetch_records', fake_fetch_items)
    def test_get_record(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertIsNotNone(self.register.get_record('1001'))
        self.assertIsNone(self.register.get_record('1002'))


class SBItemStatusRegisterTestCase(TestCase):

    def setUp(self):
        self.register = StandardBooks().item_statuses

    def test_parse_xml(self):
        records = fake_fetch_item_status(self)
        self.assertDictEqual(
            self.register.parse_xml(records),
            {
                '1001': {
                    'A': {'Code': '1001', 'Name': 'Abc', 'Location': 'A'},
                    'T': {'Code': '1001', 'Name': 'Abc', 'Location': 'T'},
                },
                '2002': {'T': {'Code': '2002', 'Name': 'Xyz', 'Location': 'T'}},
            },
        )

    @patch.object(SBRegister, 'fetch_records', fake_fetch_item_status)
    def test_get_record(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertIsNotNone(self.register.get_record('1001'))
        self.assertIsNone(self.register.get_record('1002'))

    @patch.object(SBRegister, 'fetch_records', fake_fetch_item_status)
    def test_get_record_in_location(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertIsNotNone(self.register.get_record_in_location('1001', 'A'))
        self.assertIsNone(self.register.get_record_in_location('1001', 'B'))
        self.assertIsNone(self.register.get_record_in_location('1002', 'A'))


class SBItemGroupRegisterTestCase(TestCase):

    def setUp(self):
        self.register = StandardBooks().item_groups

    @patch.object(SBRegister, 'fetch_records', fake_fetch_item_groups)
    def test_get_record(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertIsNotNone(self.register.get_record('1001'))
        self.assertIsNone(self.register.get_record('1002'))

    @patch.object(SBRegister, 'fetch_records', fake_fetch_item_groups)
    def test_get_partner_name(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertEquals(self.register.get_partner_name('1001'), 'Abc Inc')
        self.assertIsNone(self.register.get_partner_name('1002'))


class SBCustomerRegisterTestCase(TestCase):

    def setUp(self):
        self.register = StandardBooks().customers

    @patch.object(SBRegister, 'fetch_records', fake_fetch_customers)
    def test_get_record(self):
        # Force fetching records to avoid hitting cache with wrong values
        self.register.get_records(force=True)
        self.assertIsNotNone(self.register.get_record('1001'))
        self.assertIsNone(self.register.get_record('1002'))
