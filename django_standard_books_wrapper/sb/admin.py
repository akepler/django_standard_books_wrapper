from django.conf import settings
from django.contrib import admin

from sb import models


class SBCustomerAdmin(admin.ModelAdmin):
    list_display = ('code', 'name', 'reg_code')
    search_fields = ('code', 'name', 'reg_code')


# For local usage
if settings.DEBUG:
    admin.site.register(models.SBCustomer, SBCustomerAdmin)
