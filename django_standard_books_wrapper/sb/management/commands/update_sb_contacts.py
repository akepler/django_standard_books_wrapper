import logging

from django.core.management.base import BaseCommand

from oscar.core.loading import get_model

from sb.tasks import update_sb_contacts

logger = logging.getLogger(__name__)

sb_contact = get_model('sb', 'SBCustomer')


class Command(BaseCommand):
    def add_arguments(self, parser):
        parser.add_argument('--flush', action='store_true', dest='flush', default=False)

    def handle(self, *args, **options):
        if options.get('flush'):
            logger.info('Flushing old SB contacts records')
            sb_contact.objects.all().delete()

        update_sb_contacts()
