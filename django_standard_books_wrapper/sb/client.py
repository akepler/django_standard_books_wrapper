from sb import registers


class StandardBooks(object):

    _api_url = None

    @property
    def api_url(self):
        return self._api_url

    @api_url.setter
    def api_url(self,value):
        self._api_url = value

    def __init__(self,api_url=None) -> None:
        super().__init__()


        registers.base_fetch_url = self._api_url
        # Registers
        self.items = registers.SBItemRegister()
        self.item_statuses = registers.SBItemStatusRegister()
        self.item_groups = registers.SBItemGroupRegister()
        self.vat_codes = registers.SBVATCodesRegister()
        self.customers = registers.SBCustomerRegister()
        self.orders = registers.SBOrdersRegister()
