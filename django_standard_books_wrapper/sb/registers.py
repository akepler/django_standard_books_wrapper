import logging
import datetime as DT

import hashlib
import json
from collections import OrderedDict
from xml.etree import ElementTree

import re
from bs4 import BeautifulSoup

from django.core.cache import cache
from django.conf import settings

import requests

logger = logging.getLogger(__name__)

class SBRegister(object):
    CACHE_PREFIX = 'SB_CACHE'
    CACHE_TIMEOUT = 60 * 60

    REGISTER_ID = None
    RECORD_ID = None
    API_GET_PARAMS = {}

    def __init__(self) -> None:
        super().__init__()
        self.instance_cache = {}
        self._api_url = 'http://{host}:{port}/WebDataAPI.hal'.format(host=settings.SB_HOSTNAME, port=settings.SB_PORT)


    @property
    def base_fetch_url(self):
        """
        Create the URL where the data is fetched from.
        This is put together using settings.SB_HOSTNAME and settings.SB_PORT.
        """
        return self._api_url

    @base_fetch_url.setter
    def base_fetch_url(self, value):
        self._api_url = value

    def get_cache_key(self, params):
        """Create cache key to be used for a request with the specified parameters."""
        if params and isinstance(params, dict):
            params = OrderedDict(sorted(params.items(), key=lambda p: p[0]))

        params_string = ''
        if params:
            params_string = ';'.join(['{}={}'.format(*p) for p in params.items()])

        cache_hash = hashlib.sha256(
            '{url}:{params}'.format(url=self.base_fetch_url, params=params_string).encode('utf-8')
        )
        return '{prefix}:{hash}'.format(prefix=self.CACHE_PREFIX, hash=cache_hash.hexdigest())

    def parse_xml(self, xml_text):
        """Parse XML and return a dict of items where the value from RECORD_ID is used as key."""
        items = {}

        if self.RECORD_ID is None:
            return items

        root = ElementTree.fromstring(xml_text)
        for record in root.findall('./register/record/head'):
            item = {row.tag: row.text for row in record}
            key = item.get(self.RECORD_ID)
            if key is None:
                continue
            items[key] = item
        return items

    def get_params(self):
        """Get the parameters for the request. Mainly used to identify the register."""
        params = self.API_GET_PARAMS
        if self.REGISTER_ID is not None:
            params['register'] = self.REGISTER_ID
        return params

    def fetch_records(self, params=None):
        """Fetch data from StandardBooks with the specified parameters."""
        if params is None:
            params = self.get_params()
        data = requests.get(self.base_fetch_url, params, timeout=settings.SB_TIMEOUT)
        data.raise_for_status()

        return data.text

    def get_records(self, force=False):
        """Get data from StandardBooks with the specified parameters. Cache is used not to make requests too often."""
        params = self.get_params()
        cache_key = self.get_cache_key(params)
        parsed_data = None

        if not force:
            # getting the python dictionary directly is significantly faster than doing a json.load from a string
            cached_data = self.instance_cache.get(cache_key)
            parsed_data = cache.get(cache_key)
            if cached_data is not None and parsed_data:
                return cached_data

        if parsed_data is not None:
            # We got the data from cache
            data = json.loads(parsed_data)
            self.instance_cache[cache_key] = data
            return data

        xml_text = self.fetch_records(params)
        parsed_data = self.parse_xml(xml_text)
        cache.set(cache_key, json.dumps(parsed_data), self.CACHE_TIMEOUT)

        return parsed_data

    def get_record(self, key):
        """Get a record from StandardBooks using the record identifier (usually code field)

        Code field usually represents UPC for products, but there are other use cases in StandardBooks depending on the
        register.
        """
        items = self.get_records()
        return items.get(key)


class SBItemRegister(SBRegister):
    REGISTER_ID = 'INVc'
    RECORD_ID = 'Code'


class SBOrdersRegister(SBRegister):
    REGISTER_ID = 'ORVC'
    RECORD_ID = 'SerNr'

    def get_new_order_number(self):
        order_numbers = self.get_order_numbers(DT.date.today().strftime("%Y-%m-%d"))
        order_numbers.sort()
        last_order_number = order_numbers[-1]

        logger.info("Got last order nr %s, type %s",last_order_number,type(last_order_number))

        return int(last_order_number) + 1

    def get_order_numbers(self,start_date):
        self.set_api_params(start_date)
        soup = self.handle_xml()
        order_numbers = [n.get_text() for n in soup.findAll(self.RECORD_ID)]
        return order_numbers

    def set_api_params(self, start_date):
        self.API_GET_PARAMS = {
            "register": self.REGISTER_ID,
            "sd": start_date
        }

    def handle_xml(self):
        xml = self.fetch_records()
        soup = BeautifulSoup(xml, 'lxml-xml')
        return soup

    def get_order_numbers_with_customer(self,start_date,end_date):
        self.set_api_params(start_date)
        self.API_GET_PARAMS['ed']=end_date

        soup = self.handle_xml()
        order_numbers = [(n.find(self.RECORD_ID).get_text(),
                          n.find('CustCode').get_text(),
                          n.find('Addr0').get_text(),
                          n.find('OrdDate').get_text(),
                          ) for n in soup.findAll('record')]
        return order_numbers

    def get_order_rows(self,order_number,start_date):
        self.set_api_params(start_date)

        self.API_GET_PARAMS['ed']=start_date

        soup = self.handle_xml()
        order_rows = [{"article_code" : r.find("ArtCode").get_text(),
                       "spec": r.find("Spec").get_text(),
                       "serial": r.find("SerialNr").get_text(),
                       "order_quantity" : r.find("Quant").get_text(),
                       "price": r.find("Price").get_text(),
                       "sum": r.find("Sum").get_text(),
                       }
                      for r in soup.find_all(text=re.compile(order_number))[0].parent.parent.parent.rows.find_all("row")]
        return order_rows



    @property
    def base_post_url(self):
        """ Create the URL where data is posted to. """
        return 'http://{host}:{port}/WebZVVAPI.hal'.format(host=settings.SB_HOSTNAME, port=settings.SB_PORT)


class SBItemStatusRegister(SBRegister):
    REGISTER_ID = 'ItemStatusVc'
    RECORD_ID = 'Code'

    def parse_xml(self, xml_text):
        """Parse XML and return a dict of items where the value from RECORD_ID is used as key."""
        items = {}

        root = ElementTree.fromstring(xml_text)
        for record in root.findall('./register/record/head'):
            item = {row.tag: row.text for row in record}
            code = item.get(self.RECORD_ID)
            location = item.get('Location')
            if code is None or location is None:
                continue
            if code not in items:
                items[code] = {}
            items[code][location] = item
        return items

    def get_record_in_location(self, upc, location):
        """Get item statuses in the specified location from StandardBooks using UPC"""
        item = self.get_record(upc)
        if item is None:
            return None

        return item.get(location)


class SBItemGroupRegister(SBRegister):
    REGISTER_ID = 'ITVc'
    RECORD_ID = 'Code'

    def get_partner_name(self, group):
        """Get item's partner name from StandardBooks using group code

        Why is it stored in the comment???
        """
        item = self.get_record(group)
        if item is None:
            return None
        return item.get('Comment')


class SBVATCodesRegister(SBRegister):
    REGISTER_ID = 'VATCodeBlock'
    RECORD_ID = 'VATCode'


class SBCustomerRegister(SBRegister):
    REGISTER_ID = 'CUVc'
    RECORD_ID = 'Code'
