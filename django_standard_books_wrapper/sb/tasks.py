import logging

from sb.client import StandardBooks
from sb.models import SBCustomer
from zoovetvaru.celery import app


logger = logging.getLogger(__name__)


@app.task
def update_sb_contacts():
    sb = StandardBooks()
    customers = sb.customers.get_records(force=True)

    customers_created = 0
    customers_updated = 0

    for code, customer in customers.items():
        name = customer['Name']
        address_fields = ['InvAddr0', 'InvAddr1', 'InvAddr2', 'InvAddr3', 'InvAddr4']
        address = '\n'.join(filter(lambda a: a is not None, [customer.get(f) for f in address_fields]))
        vat = customer.get('VATNr') or ''
        try:
            reg_code = int(customer.get('RegNr1'))
        except (ValueError, TypeError):
            reg_code = None

        _, created = SBCustomer.objects.update_or_create(code=code, defaults={
            'name': name,
            'address': address,
            'vat': vat,
            'reg_code': reg_code,
        })

        if created:
            customers_created += 1
        else:
            customers_updated += 1

    logger.info('%s new and %s existing StandardBooks customers updated', customers_created, customers_updated)
