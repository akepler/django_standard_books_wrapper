from django.db import models
from django.utils.translation import ugettext_lazy as _


class SBCustomer(models.Model):
    code = models.CharField(_('Code in StandardBooks'), unique=True, max_length=32)
    name = models.CharField(_('Name'), max_length=255)
    address = models.TextField(_('Address'))
    vat = models.CharField(_('VAT number'), max_length=32, blank=True)
    reg_code = models.BigIntegerField(_('Registry code'), null=True)

    class Meta:
        verbose_name = _('StandardBooks customer')
        verbose_name_plural = _('StandardBooks customer')

    def __str__(self):
        return "{} ({})".format(self.code, self.name)

    @staticmethod
    def get_customer(reg_code):
        try:
            return SBCustomer.objects.get(reg_code=reg_code)
        except (SBCustomer.DoesNotExist, SBCustomer.MultipleObjectsReturned):
            return None
