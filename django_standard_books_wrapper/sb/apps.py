from django.apps import AppConfig
from django.utils.translation import ugettext_lazy as _


class SBConfig(AppConfig):
    name = 'sb'
    verbose_name = _('StandardBooks')
