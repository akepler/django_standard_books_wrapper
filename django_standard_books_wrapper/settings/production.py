from settings.staging import *


ALLOWED_HOSTS = ['demo.smartbi.eu','demo.smartbi.ee']

# Static site url, used when we need absolute url but lack request object, e.g. in email sending.
SITE_URL = 'https://demo.smartbi.eu'

EMAIL_HOST_PASSWORD = 'EI OLE'

RAVEN_BACKEND_DSN = 'https://TODO:TODO@sentry.thorgate.eu/TODO'
RAVEN_PUBLIC_DSN = 'https://TODO@sentry.thorgate.eu/TODO'
RAVEN_CONFIG['dsn'] = RAVEN_BACKEND_DSN

CSRF_COOKIE_SECURE = True
CSRF_COOKIE_HTTPONLY = True
SESSION_COOKIE_SECURE = True
